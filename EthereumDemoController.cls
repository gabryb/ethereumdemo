public without sharing class EthereumDemoController {
   	public ServiceContract contract {public  set; public get;}                                                

    public EthereumDemoController(ApexPages.StandardController standardController) {
    	this.contract = [
            SELECT Id, Name, StartDate, EndDate, GrandTotal 
            FROM ServiceContract
        	WHERE Id = :standardController.getId()
            LIMIT 1
        ];
    }
}