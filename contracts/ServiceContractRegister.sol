pragma solidity >=0.5.0 <0.6.0;

contract ServiceContractRegister {

    struct ServiceContract {
		string id;
		string name;
		string startDate;
		string endDate;
		string grandTotal;
	}

    mapping(string => ServiceContract) contractStructs;
   
    function addServiceContract(string calldata sContractId, string calldata name, string calldata startDate, string calldata endDate, string calldata grandTotal) 
    	external 
    	returns(bool success)
    {   

        contractStructs[sContractId].id = sContractId;
        contractStructs[sContractId].name = name;
        contractStructs[sContractId].startDate = startDate;
        contractStructs[sContractId].endDate = endDate;
        contractStructs[sContractId].grandTotal = grandTotal;

        return true;
    }


    function getServiceContract(string calldata sContractId) 
    	external 
    	view 
    	returns(string memory, string memory, string memory, string memory, string memory) 
    {
    	return(
            contractStructs[sContractId].id, 
            contractStructs[sContractId].name, 
            contractStructs[sContractId].startDate,
            contractStructs[sContractId].endDate,
            contractStructs[sContractId].grandTotal
        );
	} 
}


	


