const ServiceContractRegister = artifacts.require('../../contracts/ServiceContractRegister.sol');

  contract('ServiceContractRegister', function(accounts) {

    it('adds a service contract', async function () {
        const contract = await ServiceContractRegister.deployed();
        await contract.addServiceContract('x123','test name', '06-04-2019', '06-05-2019', '0.11');
        var sContract = await contract.getServiceContract('x123');
        assert.equal('test name', sContract[1], "name don't match");
    });
});
